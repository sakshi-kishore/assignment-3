import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSVtoProto {
    public void  readCsvData(String FilePath, String FileType)
    {
        try {

            FileReader fr = new FileReader(FilePath);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            boolean header = true;

            while((line = br.readLine()) != null)
            {
                if (header) {
                    // avoiding first line of csv file
                    header = false;
                    continue;
                }
                String [] data = line.split(",");

                if (FileType.equals("Employee"))
                    printEmployeeData(data);
                else
                    printBuildingData(data);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printEmployeeData(String EmployeeData[])
    {
        if(EmployeeData.length==0)
            return;

        ProtocolBuffer.EmployeeOuterClass.Employee.Builder employee=ProtocolBuffer.EmployeeOuterClass.Employee.newBuilder();
        employee.setEmployeeId(Integer.parseInt(EmployeeData[0]))
                .setName(EmployeeData[1])
                .setBuildingCode(EmployeeData[2])
                .setFloorNumberValue(Integer.parseInt(EmployeeData[3]))
                .setSalary(Integer.parseInt(EmployeeData[4]))
                .setDepartment(EmployeeData[5]);
        System.out.println(employee.toString());

    }

    private void printBuildingData(String buildingData[])
    {
        if(buildingData.length==0)
            return;
        ProtocolBuffer.BuildingOuterClass.Building.Builder building =ProtocolBuffer.BuildingOuterClass.Building.newBuilder();
        building.setBuildingCode(buildingData[0])
                .setTotalFloors(Integer.parseInt(buildingData[1]))
                .setTotalCompanies(Integer.parseInt(buildingData[2]))
                .setCafeteriaCode(buildingData[3]);
        System.out.println(building.toString());
    }
  public static void main(String args[])
  {
      String EmployeeCsvFilePath="/Users/sakshi/IdeaProjects/CapstoneProject-3/src/main/resources/Employee.csv";
      String BuildingCsvFilePath="/Users/sakshi/IdeaProjects/CapstoneProject-3/src/main/resources/Building.csv";
      CSVtoProto obj=new CSVtoProto();
      System.out.println("************************************************************************************");
      System.out.println("Employee Data:\n");
      obj.readCsvData(EmployeeCsvFilePath,"Employee");
      System.out.println("************************************************************************************");
      System.out.println("\nBuilding Data:\n");
      obj.readCsvData(BuildingCsvFilePath,"Building");

  }

}
