Following steps has been used to create this assignment.

1.  Added required protobuff dependencies and plugins in build.graddle file.
2.  Created employees and building csv files and stored them inside resources.(path - /src/main/resources/)
3.  Created employee.proto and building.proto inside proto directory. (path - /src/main/proto/).
4.  Generated proto java program inside ProtocolBuffer package.( path - /src/main/java/ProtocolBuffer/).
5.  Created 'CSVtoProto' java program to print employees and building data using proto object.(path - /src/main/java/CSVtoProto.java)
